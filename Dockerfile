FROM docker:19.03.5-git

LABEL org.label-schema.vendor="ArgoNet" \
      org.label-schema.name="docker-compose" \
      org.label-schema.url="https://gitlab.com/argonet/docker-compose" \
      org.label-schema.vcs-url="https://gitlab.com/argonet/docker-compose.git" \
      org.label-schema.maintainer="Phillip Stockmann <zyanklee@gmail.com>" \
      org.label-schema.schema-version="1.0"

COPY requirements.txt requirements.txt

RUN apk --no-cache add "python2>=2.7.16" "openssh-client>=7.9"
RUN apk --no-cache --virtual docker-compose--build-dep add \
        "build-base>=0.5" "py-pip>=18.1" "python2-dev>=2.7.16" && \
    pip install -r requirements.txt && \
    apk del --no-cache docker-compose--build-dep
